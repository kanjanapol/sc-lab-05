package lab5;

import java.util.Scanner;

public class main {
	

	public static void main(String[] args) {
		book ninjabook = new book("NiNJA BOOK", "Naruto ja");
		book javatext = new book("javatext", "Chule"); // create book
	
		user user1 = new user ("MR.Mak",1112); 
		user user2 = new user ("MR.AUM",1114); // create User
		library kulibrary = new library();  //create library
		
		kulibrary.addbook(ninjabook);
		kulibrary.addbook(javatext);// add ninja book to library
		System.out.println(kulibrary.borrowbook(user1, ninjabook));
		System.out.println(kulibrary.borrowbook(user2, javatext)); // test user1 for borrow ninja book

	}

}